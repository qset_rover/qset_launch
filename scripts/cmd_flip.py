#!/bin/python
import rospy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float32
import math

class DriveController:

    def __init__(self):
        rospy.init_node("cmd_vel_repub")

        self.cmd_pub = rospy.Publisher("/diff_drive_controller/cmd_vel", Twist, queue_size=1)
        cmd_sub = rospy.Subscriber("/cmd_vel/unfiltered", Twist, self.inCallBack, queue_size=1)
        plan_sub =rospy.Subscriber("/move_base/TrajectoryPlannerROS/local_plan", Path, self.pathCallback, queue_size=1)
        self.heading_pub = rospy.Publisher("/desired_heading", Float32, queue_size=1)

        rospy.spin()

    def inCallBack(self, msg):
        temp = Twist()
        temp.linear.x = msg.linear.x * 1 if abs(msg.angular.z) < 0.39 else 0
        temp.angular.z = msg.angular.z * 7 if abs(msg.angular.z) > 0.39 else 0
        self.cmd_pub.publish(temp)

    def pathCallback(self, msg):
        first = msg.poses[0]
        last = msg.poses[len(msg.poses) -1]
        heading = math.atan((first.pose.position.x - last.pose.position.x) / (first.pose.position.y - last.pose.position.y)) if (first.pose.position.y - last.pose.position.y) != 0 else math.pi / 2.0
        out = Float32()
        out.data = heading
        print ("Heading: %f   Len: %d", heading * 180.0 / math.pi, len(msg.poses))
        self.heading_pub.publish(out)


if __name__ == '__main__':
    driveController = DriveController()